package com.leovegas.wallet.domain;


import com.leovegas.wallet.service.dto.TransactionDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * Debit Credit transaction's which keeps the history of the user transactions.
 */
@Entity
@Table(name = "currency_transactions")
public class CurrencyTransactions implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "wallet", referencedColumnName = "id", nullable = false)
    @NotNull
    private UserWallets wallet;


    @NotNull
    @Column(name="money_amount")
    private double moneyAmount;

    @NotNull
    @Column(name="game_credit_amount")
    private double gameCreditAmount;

    @Column(name="transaction_id", unique = true)
    private String transactionId;

    @Column(name="transaction_type")
    private String transactionType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserWallets getWallet() {
        return wallet;
    }

    public void setWallet(UserWallets wallet) {
        this.wallet = wallet;
    }

    public double getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(double moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public double getGameCreditAmount() {
        return gameCreditAmount;
    }

    public void setGameCreditAmount(double gameCreditAmount) {
        this.gameCreditAmount = gameCreditAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public CurrencyTransactions() {
    }

    public CurrencyTransactions(TransactionDTO transactionDTO, UserWallets userWallets) {
        this.gameCreditAmount = transactionDTO.getGameCreditAmount();
        this.moneyAmount = transactionDTO.getMoneyAmount();
        this.wallet = userWallets;
        this.transactionType = transactionDTO.getTransactionType().toUpperCase();
        this.transactionId = transactionDTO.getTransactionId();
    }
}
