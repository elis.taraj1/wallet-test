package com.leovegas.wallet.domain;

import com.leovegas.wallet.service.dto.WalletDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * User wallets which keeps the credits of user.
 */
@Entity
@Table(name = "user_wallets")
public class UserWallets  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user", referencedColumnName = "id", nullable = false)
    @NotNull
    private ApplicationUser user;

    @NotNull
    @Column(name="amount", nullable = false)
    private double amount;

    @Column(name="currency_code", nullable = false)
    @NotNull
    private String currencyCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public UserWallets(WalletDTO walletDTO, ApplicationUser user) {
        this.currencyCode = walletDTO.getCurrencyCode().toUpperCase();
        this.amount = walletDTO.getAmount();
        this.user = user;
    }

    public UserWallets() {
    }

}
