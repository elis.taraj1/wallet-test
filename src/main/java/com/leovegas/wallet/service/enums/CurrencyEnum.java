package com.leovegas.wallet.service.enums;

/**
 * Currency enums which will be used for {@UerWallets.currencyCode} and debit transaction for {@GAME_CREDIT}
 */
public enum CurrencyEnum {
    EURO(  "EUR",   1.0),
    KORONA("SEK",       1.5),
    DOLLAR("USD",       1.5),
    LEKE("LEK",       2.0),
    GAME_CREDIT("GCR",   0.0);

    private final String currencyCode;
    private final Double priceForGameCredit;

    CurrencyEnum(String currencyCode, Double priceForGameCredit) {
        this.currencyCode = currencyCode;
        this.priceForGameCredit = priceForGameCredit;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Double getPriceForGameCredit() {
        return priceForGameCredit;
    }

    public static CurrencyEnum getEnumByCurrencyCode(String currencyCode) {
        if(EURO.getCurrencyCode().equals(currencyCode)){
            return EURO;
        } else if(KORONA.getCurrencyCode().equals(currencyCode)){
            return KORONA;
        }else if(DOLLAR.getCurrencyCode().equals(currencyCode)){
            return DOLLAR;
        }else if(LEKE.getCurrencyCode().equals(currencyCode)){
            return LEKE;
        }else if(GAME_CREDIT.getCurrencyCode().equals(currencyCode)){
            return GAME_CREDIT;
        }
        return null;
    }

}
