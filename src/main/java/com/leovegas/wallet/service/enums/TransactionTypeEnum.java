package com.leovegas.wallet.service.enums;

/**
 * Transaction Type Enums, (Credit, Debit).
 */
public enum TransactionTypeEnum {
    DEBIT,
    CREDIT
}
