package com.leovegas.wallet.service;

import com.leovegas.wallet.domain.ApplicationUser;
import com.leovegas.wallet.domain.UserWallets;
import com.leovegas.wallet.repository.ApplicationUserRepository;
import com.leovegas.wallet.repository.UserWalletsRepository;
import com.leovegas.wallet.service.dto.TransactionDTO;
import com.leovegas.wallet.service.dto.WalletDTO;
import com.leovegas.wallet.service.enums.CurrencyEnum;
import com.leovegas.wallet.service.enums.TransactionTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing UserWallets.
 */
@Service
@Transactional
public class UserWalletsService {

    private static final String applicationName = "WalletApp";

    private final Logger log = LoggerFactory.getLogger(UserWalletsService.class);

    private final UserWalletsRepository userWalletsRepository;
    private final ApplicationUserRepository userRepository;

    private final CurrencyTransactionsService currencyTransactionsService;

    public UserWalletsService(UserWalletsRepository userWalletsRepository, ApplicationUserRepository userRepository, CurrencyTransactionsService currencyTransactionsService) {
        this.userWalletsRepository = userWalletsRepository;
        this.userRepository = userRepository;
        this.currencyTransactionsService = currencyTransactionsService;
    }

    /**
     * Create's a userWallet.
     * If currencyCode provided is not on the CurrencyEnum, BadRequest
     * If Wallet already exist, BadRequest
     * If amount > 0 and transactionId is blank, BadRequest, else creates wallet with amount 0.0 and creates a credit transaction with added amount.
     * If transactionId already exist, badRequest
     * @param walletDTO
     * @return
     * @throws URISyntaxException
     */
    public ResponseEntity<WalletDTO> saveWallet(WalletDTO walletDTO) throws URISyntaxException {
        if (CurrencyEnum.getEnumByCurrencyCode(walletDTO.getCurrencyCode().toUpperCase()) == null) {
            walletDTO.setMessage("Invalid currency.");
            return ResponseEntity.badRequest()
                .headers(Utils.createAlert(applicationName, "walletManagement.currencyNotFound", walletDTO.getCurrencyCode()))
                .body(walletDTO);
        } else if (this.findOneByUserAndCurrencyCode(walletDTO) != null) {
            walletDTO.setMessage("Wallet already exist.");
            return ResponseEntity.badRequest()
                .headers(Utils.createAlert(applicationName, "walletManagement.exist", walletDTO.getCurrencyCode()))
                .body(walletDTO);
        } else {
            walletDTO.setMessage("");
            walletDTO = this.saveWalletAndCreateTransactionIfNeccesary(walletDTO);
            if(walletDTO.getMessage() == null || walletDTO.getMessage().equals("")){
                return ResponseEntity.created(new URI("/api/wallets/" + walletDTO.getCurrencyCode()))
                    .headers(Utils.createAlert(applicationName, "walletManagement.created", walletDTO.getCurrencyCode()))
                    .body(walletDTO);
            }else{
                return ResponseEntity.badRequest()
                    .headers(Utils.createAlert(applicationName, "walletManagement.transactionIdExist", walletDTO.getCurrencyCode()))
                    .body(walletDTO);
            }
        }

    }

    /**
     * Get's all User wallet for currect user except of GameCreditWallet
     * @return
     */
    public List<WalletDTO> findAllByUser(){
        List<WalletDTO> walletDTOs = userWalletsRepository.findAllByUser(getUser()).stream()
                                    .filter(uw -> !uw.getCurrencyCode().equals(CurrencyEnum.GAME_CREDIT.getCurrencyCode()))
                                    .map(w -> new WalletDTO(w)).collect(Collectors.toList());
        return walletDTOs;
    }

    /**
     * Save the wallet and create transaction ID if amount > 0;
     * @param walletDTO
     * @return
     * @throws URISyntaxException
     */
    public WalletDTO saveWalletAndCreateTransactionIfNeccesary(WalletDTO walletDTO) throws URISyntaxException {
        UserWallets userWallets = new UserWallets(walletDTO, getUser());
        userWallets.setAmount(0.0);
        userWallets = userWalletsRepository.save(userWallets);
        if(walletDTO.getAmount() > 0){
            TransactionDTO transactionDTO = new TransactionDTO();
            transactionDTO.setCurrencyCode(walletDTO.getCurrencyCode());
            transactionDTO.setGameCreditAmount(0.0);
            transactionDTO.setMoneyAmount(walletDTO.getAmount());
            transactionDTO.setTransactionType(TransactionTypeEnum.CREDIT.toString());
            ResponseEntity<TransactionDTO> transactionResponse = currencyTransactionsService.saveTransaction(transactionDTO);
            if(transactionResponse.getBody().getMessage() != null && !transactionResponse.getBody().getMessage().equals("")){
                walletDTO.setMessage(transactionResponse.getBody().getMessage());
                return walletDTO;
            }
        }
        walletDTO = new WalletDTO(userWallets);
        return walletDTO;
    }

    /**
     * Get's gameCreditWallet, if does not exist create on with amount 0.
     * @return
     */
    public WalletDTO findGameCreditWalletByUser(){
        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setCurrencyCode(CurrencyEnum.GAME_CREDIT.getCurrencyCode());
        UserWallets userWallets = this.findOneByUserAndCurrencyCode(walletDTO);
        if(userWallets == null){
            walletDTO.setAmount(0.0);
            userWallets = new UserWallets(walletDTO, this.getUser());
            userWalletsRepository.save(userWallets);
        }
        walletDTO = new WalletDTO(userWallets);
        return walletDTO;
    }

    /**
     * Find's one gameCreditAmount by currency code for the loggedin user.
     * @param walletDTO
     * @return
     */
    public UserWallets findOneByUserAndCurrencyCode(WalletDTO walletDTO){
        UserWallets userWallets = userWalletsRepository.findOneByUserAndCurrencyCode(this.getUser(), walletDTO.getCurrencyCode().toUpperCase());
        return userWallets;
    }

    /**
     * Gets the logged in user.
     * @return
     */
    public ApplicationUser getUser(){
        return  userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
