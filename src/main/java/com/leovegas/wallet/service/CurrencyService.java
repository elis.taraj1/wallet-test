package com.leovegas.wallet.service;


import com.leovegas.wallet.service.dto.CurrencyDTO;
import com.leovegas.wallet.service.enums.CurrencyEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing Currency.
 */
@Service
@Transactional
public class CurrencyService {

    private final Logger log = LoggerFactory.getLogger(CurrencyService.class);

    public CurrencyService() {
    }

    /**
     * Get all currency's;
     * @return List<CurrencyDTO>
     */
    public List<CurrencyDTO> findAll(){
        return Arrays.stream(CurrencyEnum.values()).map(c -> new CurrencyDTO(c)).collect(Collectors.toList());
    }

}
