package com.leovegas.wallet.service;

import com.leovegas.wallet.domain.ApplicationUser;
import com.leovegas.wallet.domain.CurrencyTransactions;
import com.leovegas.wallet.domain.UserWallets;
import com.leovegas.wallet.repository.ApplicationUserRepository;
import com.leovegas.wallet.repository.CurrencyTransactionsRepository;
import com.leovegas.wallet.repository.UserWalletsRepository;
import com.leovegas.wallet.service.dto.TransactionDTO;
import com.leovegas.wallet.service.enums.CurrencyEnum;
import com.leovegas.wallet.service.enums.TransactionTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service class for managing currencyTransactions.
 */
@Service
@Transactional
public class CurrencyTransactionsService {
    private final Logger log = LoggerFactory.getLogger(CurrencyTransactionsService.class);

    private static final String applicationName = "WalletApp";

    private final CurrencyTransactionsRepository currencyTransactionsRepository;
    private final ApplicationUserRepository userRepository;
    private final UserWalletsRepository userWalletsRepository;

    public CurrencyTransactionsService(CurrencyTransactionsRepository currencyTransactionsRepository, ApplicationUserRepository userRepository, UserWalletsRepository userWalletsRepository) {
        this.currencyTransactionsRepository = currencyTransactionsRepository;
        this.userRepository = userRepository;
        this.userWalletsRepository = userWalletsRepository;
    }

    /**
     * Finds all transaction regarding to this user.
     * @return List<TransactionDTO>
     */
    public List<TransactionDTO> findAllByUser(){
        return currencyTransactionsRepository.findAllByUser(this.getUser()).stream().map(t -> new TransactionDTO(t)).collect(Collectors.toList());
    }

    /**
     * Create transaction's
     * If TransactionType !in (DEBIT, Credit), badRequest.
     * If UserWallets with that provided Currency code does not exist, badRequest.
     * If User is purchesing gameCredits and amount of money provided < correspondingUserWallet,   badRequest.
     * If TransactionId is blank, badRequest.
     * If TransactionId exist, badRequest.
     * @param transactionDTO
     * @return
     * @throws URISyntaxException
     */
    public ResponseEntity<TransactionDTO> saveTransaction(TransactionDTO transactionDTO) throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        UserWallets userWallets = userWalletsRepository.findOneByUserAndCurrencyCode(getUser(),
                                            transactionDTO.getCurrencyCode().toUpperCase());
        TransactionTypeEnum transactionTypeEnum = getEnumByTransactionType(transactionDTO.getTransactionType().toUpperCase());
        if (transactionTypeEnum == null) {
            transactionDTO.setMessage("Transaction type should be CREDIT or DEBIT");
            return ResponseEntity.badRequest()
                .headers(Utils.createAlert(applicationName, "transactionManagement.transactionTypeNotFound", transactionDTO.getTransactionType()))
                .body(transactionDTO);
        } else if(userWallets == null){
            transactionDTO.setMessage("Wallet with currency " + transactionDTO.getCurrencyCode().toUpperCase() + " does not exist for this user.");
            return ResponseEntity.badRequest()
                .headers(Utils.createAlert(applicationName, "transactionManagement.walletDoesNotExist", transactionDTO.getCurrencyCode()))
                .body(transactionDTO);
        } else if(transactionTypeEnum.equals(TransactionTypeEnum.DEBIT) && userWallets.getAmount() < transactionDTO.getMoneyAmount()){
            transactionDTO.setMessage("Insuficient amount, please purchase money on wallet with currency: " + transactionDTO.getCurrencyCode().toUpperCase());
            return ResponseEntity.badRequest()
                .headers(Utils.createAlert(applicationName, "transactionManagement.insuficientAmountOnWallet", transactionDTO.getCurrencyCode()))
                .body(transactionDTO);
        } else if(transactionTypeEnum.equals(TransactionTypeEnum.DEBIT) && transactionDTO.getMoneyAmount() <= 0){
            transactionDTO.setMessage("You cannot purchase game credit with 0 amount of " + transactionDTO.getCurrencyCode().toUpperCase());
            return ResponseEntity.badRequest()
                    .headers(Utils.createAlert(applicationName, "transactionManagement.insuficientAmountOnWallet", transactionDTO.getCurrencyCode()))
                    .body(transactionDTO);
        } else {
            String transactionId =  this.generateTransactionId();
            transactionDTO.setTransactionId(transactionId);
            try{
                transactionDTO = this.manipulateSuccessTransaction(transactionDTO, userWallets);
                transactionDTO.setMessage("");
                return ResponseEntity.created(new URI("/api/currencyTransaction/" + transactionDTO.getTransactionId()))
                    .headers(Utils.createAlert(applicationName, "walletManagement.created", transactionId))
                    .body(transactionDTO);
            }catch (DataIntegrityViolationException e){
                e.printStackTrace();
                transactionDTO.setMessage("A transaction is already created with that transaction code.");
                return ResponseEntity.badRequest()
                    .headers(Utils.createAlert(applicationName, "transactionManagement.transactionIdExist", transactionId))
                    .body(transactionDTO);
            }
        }
    }

    /**
     * Check what type of transaction is,
     * if it's debit removes the amount from moneyWallet, check's if GameWallet exist, if not create a new one and add the amount of game credit that user bought.
     * if it's credit, edit's the moneyAmount by adding the purchased money amount.
     * @param transactionDTO
     * @param userWallets
     * @return
     */
    private TransactionDTO manipulateSuccessTransaction(TransactionDTO transactionDTO, UserWallets userWallets){
        transactionDTO.setMessage("");

        if(this.getEnumByTransactionType(transactionDTO.getTransactionType().toUpperCase()).equals(TransactionTypeEnum.DEBIT)){
            Double calculatedValue = Double.valueOf(userWallets.getAmount()-transactionDTO.getMoneyAmount());
            userWallets.setAmount(Math.round(calculatedValue*100.0)/100.0);
            UserWallets userGameCreditWallets = userWalletsRepository.findOneByUserAndCurrencyCode(getUser(), CurrencyEnum.GAME_CREDIT.getCurrencyCode());
            if(userGameCreditWallets == null){
                userGameCreditWallets = this.createGameWallet();
            }
            Double calculatedGameAmounts = userGameCreditWallets.getAmount() +
                                    (transactionDTO.getMoneyAmount() / CurrencyEnum.getEnumByCurrencyCode(transactionDTO.getCurrencyCode().toUpperCase()).getPriceForGameCredit());
            transactionDTO.setGameCreditAmount(calculatedGameAmounts);
            userGameCreditWallets.setAmount(Math.round(calculatedGameAmounts*100.0)/100.0);
            userWalletsRepository.save(userWallets);
            userWalletsRepository.save(userGameCreditWallets);
        }else{
            Double calculatedValue = Double.valueOf(userWallets.getAmount()+transactionDTO.getMoneyAmount());
            userWallets.setAmount(Math.round(calculatedValue*100.0)/100.0);
            transactionDTO.setGameCreditAmount(0.0);
            userWalletsRepository.save(userWallets);
        }

        CurrencyTransactions currencyTransactions = new CurrencyTransactions(transactionDTO, userWallets);
        transactionDTO = new TransactionDTO(currencyTransactionsRepository.save(currencyTransactions));
        return transactionDTO;
    }

    /**
     * create game wallet for logged in user.
     *
     * @return
     */
    public UserWallets createGameWallet(){
        UserWallets userWallets = new UserWallets();
        userWallets.setAmount(0.0);
        userWallets.setUser(this.getUser());
        userWallets.setCurrencyCode(CurrencyEnum.GAME_CREDIT.getCurrencyCode());
        return userWalletsRepository.save(userWallets);
    }

    /**
     * get's the loggedin user.
     * @return
     */
    public ApplicationUser getUser(){
        return  userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    /**
     * Get's TransactionTypeEnum by transactionType
     *
     * @param transactionType
     * @return
     */
    public static TransactionTypeEnum getEnumByTransactionType(String transactionType) {
        try {
            return TransactionTypeEnum.valueOf(transactionType);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Generate TransactionId
     */
    public String generateTransactionId(){
        String transactionId = UUID.randomUUID().toString().replace("-", "");
        return transactionId;
    }

}
