package com.leovegas.wallet.service.dto;

import com.leovegas.wallet.service.enums.CurrencyEnum;

/**
 * A DTO representing a Currency.
 */
public class CurrencyDTO {

    private String currencyCode;

    private Double  priceForGameCredit;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getPriceForGameCredit() {
        return priceForGameCredit;
    }

    public void setPriceForGameCredit(Double priceForGameCredit) {
        this.priceForGameCredit = priceForGameCredit;
    }

    public CurrencyDTO(CurrencyEnum currencyEnum) {
        this.currencyCode = currencyEnum.getCurrencyCode();
        this.priceForGameCredit = currencyEnum.getPriceForGameCredit();
    }
}
