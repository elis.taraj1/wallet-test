package com.leovegas.wallet.service.dto;

import com.leovegas.wallet.domain.CurrencyTransactions;

import javax.validation.constraints.*;

/**
 * A DTO representing a CurrencyTransaction.
 */
public class TransactionDTO {


    @Size(max = 3)
    @Pattern(regexp = "LEK|SEK|USD|EUR", flags = Pattern.Flag.CASE_INSENSITIVE)
    @NotBlank
    private String currencyCode;

    @NotBlank
    @Pattern(regexp = "DEBIT|CREDIT", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String transactionType;

    @Null
    private String transactionId;

    @NotNull
    private Double moneyAmount;

    @Null
    private Double gameCreditAmount;
    @Null
    private String message;



    public TransactionDTO() {
    }

    public TransactionDTO(CurrencyTransactions currencyTransactions) {
        this.currencyCode = currencyTransactions.getWallet().getCurrencyCode();
        this.transactionType = currencyTransactions.getTransactionType();
        this.transactionId = currencyTransactions.getTransactionId();
        this.moneyAmount = currencyTransactions.getMoneyAmount();
        this.gameCreditAmount = currencyTransactions.getGameCreditAmount();
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(Double moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Double getGameCreditAmount() {
        return gameCreditAmount;
    }

    public void setGameCreditAmount(Double gameCreditAmount) {
        this.gameCreditAmount = gameCreditAmount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
