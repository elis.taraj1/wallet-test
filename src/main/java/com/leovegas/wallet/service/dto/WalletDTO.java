package com.leovegas.wallet.service.dto;

import com.leovegas.wallet.domain.UserWallets;

import javax.validation.constraints.*;

/**
 * A DTO representing a UserWallet.
 */
public class WalletDTO {


    @Size(max = 3)
    @Pattern(regexp = "LEK|SEK|USD|EUR", flags = Pattern.Flag.CASE_INSENSITIVE)
    @NotBlank
    private String currencyCode;

    @NotNull
    private Double amount;

    @Null
    private String transactionId;
    @Null
    private String message;



    public WalletDTO() {
    }

    public WalletDTO(UserWallets userWallets) {
        this.currencyCode = userWallets.getCurrencyCode();
        this.amount = userWallets.getAmount();
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
