package com.leovegas.wallet.controller;

import com.leovegas.wallet.service.UserWalletsService;
import com.leovegas.wallet.service.dto.WalletDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing UserWallets.
 * <p>
 * This class accesses the {@link com.leovegas.wallet.domain.UserWallets} entity.
 * <p>
 */
@RestController
@RequestMapping("/api")
public class UserWalletsResource {

    private final Logger log = LoggerFactory.getLogger(UserWalletsResource.class);

    private final UserWalletsService userWalletsService;

    public UserWalletsResource(UserWalletsService userWalletsService) {
        this.userWalletsService = userWalletsService;
    }

    /**
     * {@code GET /wallets} : get all UserWallets except of Game Credits.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all WalletDTO.
     */
    @GetMapping("/wallets")
    public ResponseEntity<List<WalletDTO>> getAllByUser() {
        final List<WalletDTO> userWallets = userWalletsService.findAllByUser();
        return new ResponseEntity<>(userWallets, HttpStatus.OK);
    }

    /**
     * {@code GET /gameCreditWallet} : get GameCreditWallet.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body WalletDTO.
     */
    @GetMapping("/gameCreditWallet")
    public ResponseEntity<WalletDTO> getGameCreditWalletByUser() {
        final WalletDTO userWallets = userWalletsService.findGameCreditWalletByUser();
        return new ResponseEntity<>(userWallets, HttpStatus.OK);
    }

    /**
     * {@code POST  /wallets}  : Creates a new wallets.
     * <p>
     * Creates a new UserWallet if the Currency Code is not used
     *
     * @param walletDTO the UserWallet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new walletDTO, or with status {@code 400 (Bad Request)}
     * @throws URISyntaxException       if the Location URI syntax is incorrect.
     */
    @PostMapping("/wallets")
    public ResponseEntity<WalletDTO> createUserWallet(@Valid @RequestBody WalletDTO walletDTO) throws URISyntaxException {
        log.debug("REST request to save UserWallets : {}", walletDTO);
        return userWalletsService.saveWallet(walletDTO);
    }
}
