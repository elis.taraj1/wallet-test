package com.leovegas.wallet.controller;

import com.leovegas.wallet.service.CurrencyTransactionsService;
import com.leovegas.wallet.service.dto.TransactionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing currencyTransactions.
 * <p>
 * This class accesses the {@link com.leovegas.wallet.domain.CurrencyTransactions} entity.
 * <p>
 */
@RestController
@RequestMapping("/api")
public class CurrencyTransactionsResource {

    private final Logger log = LoggerFactory.getLogger(CurrencyTransactionsResource.class);

    private final CurrencyTransactionsService currencyTransactionsService;

    public CurrencyTransactionsResource(CurrencyTransactionsService currencyTransactionsService) {
        this.currencyTransactionsService = currencyTransactionsService;
    }

    /**
     * {@code GET /currencyTransaction} : get all CurrencyTransactions for the loggegin user.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all CurrencyTransactionsDTO.
     */
    @GetMapping("/currencyTransaction")
    public ResponseEntity<List<TransactionDTO>> getAllByUser() {
        final List<TransactionDTO> currencyTransactions = currencyTransactionsService.findAllByUser();
        return new ResponseEntity<>(currencyTransactions, HttpStatus.OK);
    }

    /**
     * {@code POST  /currencyTransaction}  : Creates a new CurrencyTransaction.
     * <p>
     * Creates a new CurrencyTransaction if the TransactionId is not used and user have amount in that wallet
     *
     * @param transactionDTO the CurrencyTransaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionDTO, or with status {@code 400 (Bad Request)}
     * @throws URISyntaxException       if the Location URI syntax is incorrect.
     */
    @PostMapping("/currencyTransaction")
    public ResponseEntity<TransactionDTO> createTransaction(@Valid @RequestBody TransactionDTO transactionDTO) throws URISyntaxException {
        log.debug("REST request to save CurrencyTransaction : {}", transactionDTO);
        return currencyTransactionsService.saveTransaction(transactionDTO);
    }
}
