package com.leovegas.wallet.controller;

import com.leovegas.wallet.service.CurrencyService;
import com.leovegas.wallet.service.dto.CurrencyDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Currency.
 * <p>
 * This class accesses the Currency.
 * <p>
 */
@RestController
@RequestMapping("/api")
public class CurrencyResource {

    private final Logger log = LoggerFactory.getLogger(CurrencyResource.class);

    private final CurrencyService currencyService;

    public CurrencyResource(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    /**
     * {@code GET /currency} : get all availeble currency's.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all currencyCodes which user does not have a wallet with.
     */
    @GetMapping("/currency")
    public ResponseEntity<List<CurrencyDTO>> getAllCurrencys() {
        final List<CurrencyDTO> currency = currencyService.findAll();
        return new ResponseEntity<>(currency, HttpStatus.OK);
    }

}
