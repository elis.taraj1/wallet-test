package com.leovegas.wallet.controller;

import com.leovegas.wallet.domain.ApplicationUser;
import com.leovegas.wallet.repository.ApplicationUserRepository;
import com.leovegas.wallet.service.Utils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserResource {

    private static final String applicationName = "WalletApp";
    private ApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserResource(ApplicationUserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<String> signUp(@RequestBody ApplicationUser user) {
        Boolean userExist = applicationUserRepository.findByUsername(user.getUsername()) != null;
        if(userExist){
            return ResponseEntity.badRequest()
                    .headers(Utils.createAlert(applicationName, "userManagment.exist", user.getUsername())).body("User exist");
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
        return ResponseEntity.ok().body("");
    }
}