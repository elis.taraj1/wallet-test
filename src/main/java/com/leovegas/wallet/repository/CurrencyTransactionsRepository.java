package com.leovegas.wallet.repository;

import com.leovegas.wallet.domain.ApplicationUser;
import com.leovegas.wallet.domain.CurrencyTransactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the {@link CurrencyTransactions} entity.
 */
@Repository
public interface CurrencyTransactionsRepository extends JpaRepository<CurrencyTransactions, Long> {

    @Query("select currencyTransactions "
        + "from CurrencyTransactions currencyTransactions "
        + "  left join fetch currencyTransactions.wallet wallet "
        + "  left join fetch wallet.user user "
        + "where user = :user")
    List<CurrencyTransactions> findAllByUser(@Param("user") ApplicationUser user);

    CurrencyTransactions findOneByTransactionId(String transactionId);
}
