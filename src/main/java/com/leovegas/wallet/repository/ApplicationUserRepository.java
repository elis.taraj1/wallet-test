package com.leovegas.wallet.repository;

import com.leovegas.wallet.domain.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link ApplicationUser} entity.
 */
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
        ApplicationUser findByUsername(String username);
}
