package com.leovegas.wallet.repository;

import com.leovegas.wallet.domain.ApplicationUser;
import com.leovegas.wallet.domain.UserWallets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the {@link com.leovegas.wallet.domain.UserWallets} entity.
 */
@Repository
public interface UserWalletsRepository extends JpaRepository<UserWallets, Long> {

    List<UserWallets> findAllByUser(ApplicationUser user);

    UserWallets findOneByUserAndCurrencyCode(ApplicationUser user, String currencyCode);
}
